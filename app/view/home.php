<div id="welcome">
	<div class="row">
		<div class="container clearfix">
			<div class="wlcLeft col-6 fl">
				<div class="textBox">
					<h2>Welcome</h2>
					<p>Lu’s Jewelry is owned and managed by Ms. Lu. She has been at the same location for over 20 years and truly enjoys seeing all of her long-time friends and customers as well as meeting new ones.</p>
					<p>There are two jewelry stores located inside of the Fiesta Mart, make sure you come to the second store, us, to get the best quality jewelry for your money!</p>
					<a href="about#content" class="btn">Learn More</a>
				</div>
			</div>
			<div class="wlcRight col-6 fr">
				<img src="public/images/content/welcomeImg.jpg" alt="storefront" class="wlcIMG">
			</div>
		</div>
	</div>
</div>
<div id="gettoknow">
	<div class="row">
		<div class="gtkLeft col-6 fl">
			<div class="image">
				<img src="public/images/content/gtk1.jpg" alt="Get to Know 1">
				<p>Gold Jewelry & Accessories</p>
			</div>
			<div class="image">
				<img src="public/images/content/gtk2.jpg" alt="Get to Know 2">
				<p>Diamond Services & Sales</p>
			</div>
			<div class="image">
				<img src="public/images/content/gtk3.jpg" alt="Get to Know 3">
				<p>Watch Batteries</p>
			</div>
			<div class="image">
				<img src="public/images/content/gtk4.jpg" alt="Get to Know 4">
				<p>Jewelry Repair</p>
			</div>
		</div>
		<div class="gtkRight col-6 fr">
			<div class="textBox">
				<h2>Get to Know <span>Our Team</span></h2>
				<p>Her son, Victor, and manager of over 16 years, Maria, help with the jewelry repairs and everyday operations. Stop in today to meet our friendly staff. We have 66 years of combined industry experience!</p>
				<a href="services#content" class="btn">View All Services</a>
			</div>
		</div>
		</div>
	</div>
</div>
<div id="weBuy">
	<div class="row">
		<div class="wbLeft col-6 fl">
			<div class="textBox">
				<h2>We Buy Gold!</h2>
				<p>Bring in your scrap gold and jewelry! We also sell diamonds, 10k and 14k gold, silver, and watches. We also provide ear piercing services! Layaway is available.</p>
				<a href="services#content" class="btn">Learn More</a>
				<p class="call">Call Us Today <?php $this->info(["phone","tel","wbPhone"]); ?></p>
			</div>
		</div>
		<div class="wbRight col-6 fr">
			<img src="public/images/content/ring.png" alt="ring" class="ring">
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="section4">
	<div class="row">
		<div class="s4Left col-4 fl">
			<h2>Professional Jewelry Repair</h2>
		</div>
		<div class="s4Right col-8 fr">
			<div class="textBox">
				<p>Don’t throw away your broken jewelry - bring it into the professionals at Lu’s Jewelry for quality repairs! Whether you need a ring resized, our family-owned company is your go-to source for all your jewelry repair needs. Contact us to learn more today!</p>
				<a href="services#content" class="btn">Learn More</a>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="collection">
	<div class="row">
		<div class="header">
			<h2>View Our Collection</h2>
			<a href="gallery#content" class="btn2">View More</a>
		</div>
		<div class="container">
			<div class="colTop">
				<img src="public/images/content/col1.jpg" alt="Collection 1">
				<img src="public/images/content/col2.jpg" alt="Collection 2">
				<img src="public/images/content/col3.jpg" alt="Collection 3">
			</div>
			<div class="colBot">
				<img src="public/images/content/col4.jpg" alt="Collection 4">
				<img src="public/images/content/col5.jpg" alt="Collection 5">
				<img src="public/images/content/col6.jpg" alt="Collection 6">
			</div>
		</div>
	</div>
</div>
<div id="contact">
	<div class="row">
		<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
			<div class="cntLeft col-5 fl">
				<h2>Contact Form</h2>
				<p>We will be glad to answer your questions, feel free to ask a piece of information or a quotation. We are looking forward to work with you.</p>
				<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
				<div class="g-recaptcha"></div>
				<label>
					<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
				</label><br>
				<?php if( $this->siteInfo['policy_link'] ): ?>
				<label>
					<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
				</label>
				<?php endif ?>
				<button type="submit" class="ctcBtn btn" disabled>Submit Form</button>
			</div>
			<div class="cntRight col-7 fl">
				<div class="cntTopRight">
					<label><span class="ctc-hide">Name</span>
						<input type="text" name="name" placeholder="Name:">
					</label>
					<label><span class="ctc-hide">Email</span>
						<input type="text" name="email" placeholder="Email:">
					</label>
				</div>
				<div class="cntMidRight">
					<label><span class="ctc-hide">Phone</span>
						<input type="text" name="phone" placeholder="Phone:">
					</label>
					<label><span class="ctc-hide">Confirm Email</span>
						<input type="text" name="c-email" placeholder="Confirm Email:">
					</label>
				</div>
				<label><span class="ctc-hide">Message</span>
					<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
				</label>
			</div>
			<div class="clearfix"></div>
		</form>
	</div>
</div>
