<footer>
	<div id="footer">
		<div class="footTop">
			<div class="row">
				<a href="<?php echo URL; ?>">
					<img src="public/images/common/ftLogo.png" alt="Foot Logo" class="ftLogo">
				</a>
				<div class="footRight col-4 fr">
					<h2>Contact Us</h2>
					<div class="info col-11 fl">
						<h5>Phone</h5>
						<p><?php $this->info(["phone","tel","ftPhone"]);?></p>
						<h5>Email</h5>
						<p><?php $this->info(["email","mailto"]); ?></p>
						<h5>Location</h5>
						<p><?php $this->info("address"); ?></p>
						<p class="italic">(We are located inside the Fiesta Mart. There are <br>two jewelry stores inside and we are the second one)</p>
						<h5>Business Hours</h5>
						<p><?php $this->info("hours"); ?></p>
					</div>
					<div class="social col-1 fr">
						<p>
							<a href="<?php $this->info("fb_link"); ?>" class="socialico">f</a>
							<a href="<?php $this->info("tt_link"); ?>" class="socialico">l</a>
							<a href="<?php $this->info("yt_link"); ?>" class="socialico">x</a>
							<a href="<?php $this->info("rsss_link"); ?>" class="socialico">r</a>
						</p>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="footBot">
			<div class="row">
				<div class="ftBotLeft col-6 fl">
					<ul>
						<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">Home</a></li>
						<li <?php $this->helpers->isActiveMenu("about"); ?>><a href="<?php echo URL ?>about#content">About Us </a></li>
						<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services#content">Services</a></li>
						<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery#content">Gallery</a></li>
						<li <?php $this->helpers->isActiveMenu("testimonials"); ?>><a href="<?php echo URL ?>testimonials#content">Testimonials</a></li>
						<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact#content">Contact Us</a></li>
					</ul>
				</div>
				<div class="ftBotRight col-6 fr">
					<p class="copy">
						© <?php echo date("Y"); ?>. <?php $this->info("company_name"); ?> All Rights Reserved.
						<?php if( $this->siteInfo['policy_link'] ): ?>
							<a href="<?php $this->info("policy_link"); ?>">Privacy Policy</a>.
						<?php endif ?>
					</p>
					<p class="silver"><img src="public/images/scnt.png" alt="" class="company-logo" /><a href="https://silverconnectwebdesign.com/website-development" rel="external" target="_blank">Web Design</a> Done by <a href="https://silverconnectwebdesign.com" rel="external" target="_blank">Silver Connect Web Design</a></p>
				</div>
				</div>
				<div class="clearfix"></div>
		</div>
	</div>
</footer>
<textarea id="g-recaptcha-response" class="destroy-on-load"></textarea>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo URL; ?>public/scripts/sendform.js" data-view="<?php echo $view; ?>" id="sendform"></script>
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>  -->
<script src="<?php echo URL; ?>public/scripts/responsive-menu.js"></script>
<script src="https://unpkg.com/sweetalert2@7.20.10/dist/sweetalert2.all.js"></script>

<?php if( $this->siteInfo['cookie'] ): ?>
	<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
	<script src="<?php echo URL; ?>public/scripts/cookie-script.js"></script>
	<script>
	window.addEventListener("load", function(){
	window.cookieconsent.initialise({
	  "palette": {
	    "popup": {
	      "background": "#000"
	    },
	    "button": {
	      "background": "#3085d6"
	    }
	  }
	})});
	</script>
<?php endif ?>

<?php if(in_array($view,["home","contact"])): ?>
	<script src='//www.google.com/recaptcha/api.js?onload=captchaCallBack&render=explicit' async defer></script>
	<script>
		var captchaCallBack = function() {
			$('.g-recaptcha').each(function(index, el) {
				var recaptcha = grecaptcha.render(el, {'sitekey' : '<?php $this->info("site_key");?>'});
				$( '.destroy-on-load' ).remove();
			})
		};

		$('.consentBox').click(function () {
		    if ($(this).is(':checked')) {
		    	if($('.termsBox').length){
		    		if($('.termsBox').is(':checked')){
		        		$('.ctcBtn').removeAttr('disabled');
		        	}
		    	}else{
		        	$('.ctcBtn').removeAttr('disabled');
		    	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

		$('.termsBox').click(function () {
		    if ($(this).is(':checked')) {
	    		if($('.consentBox').is(':checked')){
	        		$('.ctcBtn').removeAttr('disabled');
	        	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

	</script>

<?php endif; ?>


<?php if ($view == "gallery"): ?>
	<script type="text/javascript" src="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
	<script type="text/javascript" src="<?php echo URL; ?>public/scripts/jquery.pajinate.js"></script>
	<script>
		$('#gall1').pajinate({ num_page_links_to_display : 3, items_per_page : 10 });
		$('.fancy').fancybox({
			helpers: {
				title : {
					type : 'over'
				}
			}
		});
	</script>
<?php endif; ?>

<a class="cta" href="tel:<?php $this->info("phone") ;?>"><span class="ctc-hide">Call To Action Button</span></a>

<?php $this->checkSuspensionFooter(); ?>
</body>
</html>
